# Mobilizon container image 

This dockerfile builds a container for Mobilizon.

## Presentation

"Mobilizon is a tool that helps you find, create and organize events. You can also create a page for your group where the members will be able to get organized together. As an ethical alternative to Facebook events, groups and pages, Mobilizon is a tool designed to serve you. Period. No likes, no follows, no infinite wall to scroll: Mobilizon leaves you in control of your attention. Mobilizon is not a giant platform, but a multitude of interconnected Mobilizon websites. This federated architecture helps avoid monopolies and offers a diversity of hosting terms of service."

[Source](joinmobilizon.org/en/)

## Usage

Build command example

```
docker build -t local/mobilizon:4.1.0 --build-arg="MOBILIZON_VERSION=4.1.0" --progress=plain .
```

## Links

- [Official web site](https://joinmobilizon.org/fr/)
- [Framagit repo](https://framagit.org/framasoft/mobilizon)
