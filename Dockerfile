# Mobilizon Dockerfile
#
# https://framagit.org/framasoft/mobilizon 
# Original author: Framasoft
# Adapted by PCLL Team for Eole³ project

##
# Build app
##

# First build the application assets
FROM node:20-alpine AS assets

ARG MOBILIZON_VERSION=4.1.0
ENV NPM_VERSION=10.8.1
ARG GITREPO_URL="https://gitlab.mim-libre.fr/infrabricks/containers/mobilizon/mobilizon-sources.git"

RUN apk add --no-cache python3 build-base libwebp-tools bash imagemagick ncurses git

WORKDIR /src

RUN cd /opt && git clone --depth 1 --branch ${MOBILIZON_VERSION} \
    -c advice.detachedHead=false ${GITREPO_URL} && \
    cp -r $(basename -s .git "${GITREPO_URL}")/* /src

# Network timeout because it's slow when cross-compiling
RUN npm install -g npm@${NPM_VERSION} && npm install && npm run build
# Then, build the application binary

FROM elixir:1.16-alpine AS builder

# Fix qemu segfault on arm64
# See https://github.com/plausible/analytics/pull/2879 and https://github.com/erlang/otp/pull/6340
ARG ERL_FLAGS=""
ENV ERL_FLAGS=$ERL_FLAGS

RUN apk add --no-cache build-base git cmake

WORKDIR /src

COPY --from=assets /src/mix.exs ./
COPY --from=assets /src/mix.lock ./

ENV MIX_ENV=prod
RUN mix local.hex --force \
    && mix local.rebar --force \
    && mix deps.get

COPY --from=assets /src/lib ./lib
COPY --from=assets /src/priv ./priv
COPY --from=assets /src/config/config.exs /src/config/prod.exs ./config/
COPY --from=assets /src/config/docker.exs ./config/runtime.exs
COPY --from=assets /src/rel ./rel
COPY --from=assets /src/support ./support

RUN mix phx.digest.clean --all && mix phx.digest && mix release
# Finally setup the app

##
# Final stage
##

FROM alpine
ARG BUILD_DATE
ARG VCS_REF

LABEL org.opencontainers.image.title="mobilizon" \
    org.opencontainers.image.description="Mobilizon for Docker" \
    org.opencontainers.image.vendor="joinmobilizon.org" \
    org.opencontainers.image.documentation="https://docs.joinmobilizon.org" \
    org.opencontainers.image.licenses="AGPL-3.0" \
    org.opencontainers.image.source="https://framagit.org/framasoft/mobilizon" \
    org.opencontainers.image.url="https://joinmobilizon.org" \
    org.opencontainers.image.maintainer="PCLL Team"

RUN apk add --no-cache curl openssl ca-certificates ncurses-libs file postgresql-client libgcc libstdc++ imagemagick python3 py3-pip py3-pillow py3-cffi py3-brotli gcc g++ musl-dev python3-dev pango libxslt-dev ttf-cantarell
RUN pip --no-cache-dir install --break-system-packages weasyprint pyexcel-ods3

# Create every data directory
RUN mkdir -p /var/lib/mobilizon/uploads && chown nobody:nobody /var/lib/mobilizon/uploads
RUN mkdir -p /var/lib/mobilizon/timezones && chown nobody:nobody /var/lib/mobilizon/timezones
RUN mkdir -p /var/lib/mobilizon/tzdata && chown nobody:nobody /var/lib/mobilizon/tzdata
RUN mkdir -p /var/lib/mobilizon/sitemap && chown nobody:nobody /var/lib/mobilizon/sitemap
RUN mkdir -p /var/lib/mobilizon/uploads/exports/{csv,pdf,ods} && chown -R nobody:nobody /var/lib/mobilizon/uploads/exports

# Create configuration directory
RUN mkdir -p /etc/mobilizon && chown nobody:nobody /etc/mobilizon

USER nobody

# Get timezone geodata
RUN curl -L 'https://packages.joinmobilizon.org/tz_world/timezones-geodata.dets' -o /var/lib/mobilizon/timezones/timezones-geodata.dets

EXPOSE 4000
ENV MOBILIZON_DOCKER=true
COPY --from=builder --chown=nobody:nobody /src/_build/prod/rel/mobilizon ./
COPY --from=assets --chown=nobody:nobody /src/docker/production/docker-entrypoint.sh ./
RUN cp /releases/*/runtime.exs /etc/mobilizon/config.exs

ENTRYPOINT ["./docker-entrypoint.sh"]
